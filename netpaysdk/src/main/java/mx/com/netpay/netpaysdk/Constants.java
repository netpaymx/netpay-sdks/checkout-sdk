package mx.com.netpay.netpaysdk;

final public class Constants {

    public static final int TOKEN_REQUEST = 357;
    static final String ERROR_CAST = "Context is not instance of Activity";
    static final String PARAMENTER_IS_TEST = "PARAMENTER_IS_TEST";
    static final String PARAMENTER_PK = "PARAMENTER_PK";


    public static final String RESPONSE_TOKEN = "RESPONSE_TOKEN";
    public static final String RESPONSE_LAST_FOUR = "RESPONSE_LAST_FOUR";
    public static final String RESPONSE_BRAND = "RESPONSE_BRAND";
    public static final String RESPONSE_ERROR = "RESPONSE_ERROR";

}
