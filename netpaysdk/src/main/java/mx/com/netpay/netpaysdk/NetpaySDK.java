package mx.com.netpay.netpaysdk;

import android.content.Context;

public interface NetpaySDK {

    void launchCreditCardForm(final Context context, final String publicKey, final boolean testMode);

}
