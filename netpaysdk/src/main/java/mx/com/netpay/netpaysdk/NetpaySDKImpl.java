package mx.com.netpay.netpaysdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

final class NetpaySDKImpl implements NetpaySDK {

    @Override
    public void launchCreditCardForm(final Context context, final String publicKey, final boolean testMode) {
        if(!(context instanceof Activity)) {
            throw new IllegalArgumentException(Constants.ERROR_CAST);
        }
        Intent i = new Intent(context, CardActivity.class);
        i.putExtra(Constants.PARAMENTER_IS_TEST,testMode);
        i.putExtra(Constants.PARAMENTER_PK,publicKey);

        ((Activity) context).startActivityForResult(i, Constants.TOKEN_REQUEST);
    }
}
