package mx.com.netpay.netpaysdk;

import mx.com.netpay.netpaysdk.model.TokenCardRequest;
import mx.com.netpay.netpaysdk.model.TokenCardResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

interface Api {

    @POST("v3/token")
    @Headers({"Content-Type: application/json"})
    Call<TokenCardResponse> requestCardToken(@Header("Authorization") String  authorization, @Body TokenCardRequest token);

}
