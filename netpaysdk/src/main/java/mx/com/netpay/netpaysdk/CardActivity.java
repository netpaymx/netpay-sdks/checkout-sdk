package mx.com.netpay.netpaysdk;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import mx.com.netpay.netpaysdk.model.TokenCardRequest;
import mx.com.netpay.netpaysdk.model.TokenCardResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static mx.com.netpay.netpaysdk.Constants.RESPONSE_BRAND;
import static mx.com.netpay.netpaysdk.Constants.RESPONSE_ERROR;
import static mx.com.netpay.netpaysdk.Constants.RESPONSE_LAST_FOUR;
import static mx.com.netpay.netpaysdk.Constants.RESPONSE_TOKEN;

public final class CardActivity extends AppCompatActivity {

    private EditText etCardNo;
    private EditText etName;
    private EditText etDate;
    private EditText etCvv;

    private TextInputLayout loCardNo;
    private TextInputLayout loDate;
    private TextInputLayout loCvv;

    private Button btnPay;

    private Api api;

    private String pk;

    private static final String maskVisa = "\\b(\\d{4})(\\d{4})(\\d{4})(\\d{4})\\b";
    private static final String maskAmex = "\\b(\\d{4})(\\d{6})(\\d{5})\\b";


    ProgressDialog progress;

    private static final String maskVisaFormat = "$1 $2 $3 $3";
    private static final String maskAmexFormat = "$1 $2 $3";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        boolean testMode = getIntent().getBooleanExtra(Constants.PARAMENTER_IS_TEST, true);
        pk = getIntent().getStringExtra(Constants.PARAMENTER_PK);


        etCardNo = (EditText) findViewById(R.id.etCardNo);
        etName = (EditText) findViewById(R.id.etName);
        etDate = (EditText) findViewById(R.id.etDate);
        etCvv = (EditText) findViewById(R.id.etCvv);

        loCardNo = (TextInputLayout) findViewById(R.id.loCardNo);
        loDate = (TextInputLayout) findViewById(R.id.loDate);
        loCvv = (TextInputLayout) findViewById(R.id.loCvv);

        btnPay = (Button) findViewById(R.id.btnSend);

        InputFilter[] editFilters = etName.getFilters();
        InputFilter[] newFilters = new InputFilter[editFilters.length + 1];
        System.arraycopy(editFilters, 0, newFilters, 0, editFilters.length);
        newFilters[editFilters.length] = new InputFilter.AllCaps();
        etName.setFilters(newFilters);

        etDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start == 1 && count == 1) {
                    String mes = etDate.getText().toString().trim();
                    etDate.setText(mes + "/");
                    etDate.setSelection(3);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                api = initRetrofit(testMode);
                loCardNo.setError(null);
                loDate.setError(null);
                loCvv.setError(null);
                if(validateFields()) {
                    Log.d("validateFields","validateFields");
                    sendRequest(v);
                }
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bundle b = new Bundle();
        b.putString(RESPONSE_ERROR,"Cancelado por el usuario");
        Intent i = new Intent();
        i.putExtras(b);
        setResult(Activity.RESULT_CANCELED, i);
    }

    public boolean isValid(String cardNumber){
        if(cardNumber.isEmpty()) {
            return false;
        }
        int sum = 0 ;
        boolean even = false;
        String  cardType = null;

        int firstDigit = getDigit(cardNumber,1);
        switch (firstDigit){
            case 4:
                cardType = "Visa";
                break;
            case 3:
                if (getDigit(cardNumber,2)==7) {
                    cardType= "American Express";
                } else {
                    return false;
                }
            case 5:
                cardType = "Master Card";
                break;
            case 6:
                cardType = "Discover";
                break;
        } //end card type switch

        int secondDigit = getDigit(cardNumber, 2);

        //loops through digits in reverse order right to left
        for (int i = cardNumber.length();i>0; i--){
            int digit = getDigit(cardNumber, i);

            //double every other digit
            if (even)
                digit += digit;

            even = ! even;

            //if result is greater than 9 then subtract 9
            if (digit > 9)
                digit = digit - 9;

            sum += digit;
        } //end of loop

        if (sum % 10 == 0) {
            System.out.println("is valid");
            System.out.println("Card Type: "+cardType);
            return true;
        }
        else {
            System.out.println(" is invalid");
            System.out.println("Card Type: Invalid");
            return false;
        }
    } //end of isValid class

    //gets digit at specified position
    private static int getDigit(String digitString, int position){
        String characterAtPosition = digitString.substring(position - 1, position);
        return Integer.parseInt(characterAtPosition);
    }

    private boolean validateFields() {

        boolean isValid = true;

        String cardNo = etCardNo.getText().toString();
        if(cardNo.length() < 15 && !isValid(cardNo)) {
            isValid = false;
            loCardNo.setError(getApplicationContext().getResources().getString(R.string.str_invalidcard));
        }

        if(etDate.getText().toString().length() < 5) {
            isValid = false;
            loDate.setError(getApplicationContext().getResources().getString(R.string.str_invaliddate));
        } else {
            String date = etDate.getText().toString();
            String month = date.split("/")[0];
            String year = date.split("/")[1];

            if(Integer.parseInt(month) < 1 || Integer.parseInt(month) > 12) {
                loDate.setError(getApplicationContext().getResources().getString(R.string.str_invaliddate));
                isValid = false;
            }

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH,1);
            calendar.set(Calendar.MONTH,Integer.parseInt(month));
            calendar.set(Calendar.YEAR,Integer.parseInt("20"+year));

            Date expDate = calendar.getTime();

            if(new Date().compareTo(expDate) >= 0) {
                loDate.setError(getApplicationContext().getResources().getString(R.string.str_exp_card));
                isValid = false;
            }

        }

        if(etCvv.getText().toString().length() < 3) {
            loCvv.setError(getApplicationContext().getResources().getString(R.string.str_invalidcvv));
            isValid = false;
        }


        return isValid;
    }

    private void sendRequest(View v) {
        progress = ProgressDialog.show(this, "Por favor espere...",
                "Generando token", true);

        TokenCardRequest tokenCardRequest = new TokenCardRequest();
        tokenCardRequest.setCardNumber(etCardNo.getText().toString());
        tokenCardRequest.setCardHolderName(etName.getText().toString());
        tokenCardRequest.setCvv2(etCvv.getText().toString());

        tokenCardRequest.setExpMonth(etDate.getText().toString().split("/")[0]);
        tokenCardRequest.setExpYear(etDate.getText().toString().split("/")[1]);

        api.requestCardToken(pk,tokenCardRequest).enqueue(new Callback<TokenCardResponse>() {
            @Override
            public void onResponse(Call<TokenCardResponse> call, Response<TokenCardResponse> response) {

                if(response.isSuccessful()) {
                    TokenCardResponse tokenCardResponse = response.body();

                    Bundle b = new Bundle();
                    b.putString(RESPONSE_TOKEN,tokenCardResponse.getToken());
                    b.putString(RESPONSE_LAST_FOUR,tokenCardResponse.getLastFourDigits());
                    b.putString(RESPONSE_BRAND,tokenCardResponse.getBrand());

                    Intent i = new Intent();
                    i.putExtras(b);

                    setResult(RESULT_OK,i);
                    finish();
                } else {
                    TokenCardResponse tokenCardResponse = null;
                    String errorMessage;
                    try {
                        tokenCardResponse = new Gson().fromJson(response.errorBody().string(), TokenCardResponse.class);
                        errorMessage = tokenCardResponse.getMessage();
                    } catch (IOException e) {
                        errorMessage = "Ocurrió un error al obtener el token";
                    }

                    Snackbar.make(v,errorMessage,Snackbar.LENGTH_LONG).show();
                }
                progress.dismiss();
            }

            @Override
            public void onFailure(Call<TokenCardResponse> call, Throwable t) {
                Log.d("onFailure",t.getMessage());
                Snackbar.make(v,t.getMessage(),Snackbar.LENGTH_LONG).show();
                progress.dismiss();
            }
        });
    }


    private static Api initRetrofit(boolean testMode) {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build();

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(testMode?"https://ecommerce.netpay.com.mx/gateway-ecommerce/":"https://suite.netpay.com.mx/gateway-ecommerce/");

        retrofitBuilder.client(client);
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = retrofitBuilder.build();

        return retrofit.create(Api.class);
    }
}
